#include <iostream>
#include "list.h"

using namespace std;

List::~List()
 {
	for(Node *p; !isEmpty(); )
	{
		p=head->next;
		delete head;
		head=p;
	}
}

void List::headPush(int x)
{
	Node *temp = new Node(x);
	if(isEmpty()==true)
	{
		head = temp;
		tail = temp;
	}else{
		Node *h = head;
		head = temp;
		head->next = h;
		h->previous = head;
	}
}

void List::tailPush(int x)
{
	Node *temp = new Node(x);
	if(isEmpty()==true)
	{
		head = temp;
		tail = temp;
	}else{
		Node *t = tail;
		tail = temp;
		t->next = tail;
		tail->previous = t;
	}
}

int List::headPop()
{
	Node* h = head;
	head = head->next;
	head->previous = 0;
	int r = h->info;
	delete h;
	return r;
}

int List::tailPop()
{
	Node* t = tail;
	tail=tail->previous;
	tail->next=0;
	int r = t->info;
	delete t;
	return r;
}

void List::deleteNode(int x){
	Node* current = head;
	bool has = false;
	while(1){
		
		if(current->info == x){
			has = true;
		}
		if(has == true){
			if(current == tail){
				tail = current->previous;
				tail->next = 0;
			}
			else if(current == head){
				head = current->next;
				head->previous = 0;
			}
			else{
				current->previous->next = current->next;
				current->next->previous = current->previous;
			}
			Node* del = current;
			if(current->next == 0){
				break;
			}
			else{
				current = current->next;
			}
			delete del;
			has = false;
		}
		else{
			if(current->next == 0){
				break;
			}
			else{
				current = current->next;
			}
		}
	}
}

bool List::isInList(int x)
{
	Node* current = head;
	bool has = false;
	while(1){
		if(current->info == x){
			
			has = true;
		}
		if(current->next != 0)
		{
			current = current->next;
		}
		else{
			break;
		}
	}
	return has;
}

void List::display()
{
	Node* current = head;
	cout<<"List : ";
	while(1){
		cout<<current->info<<" ";
		if(current->next == 0)
		{
			break;
		}
		else{
			current = current->next;
		}
	}
	cout<<endl;
}
