#include <iostream>
#include "list.h"
#include "list.cpp"

int main()
{
     
     List game;
     
     for(int i=1;i<=5;i++){
         game.tailPush(i);
     }
    
     game.display();
    
     cout<<"Add 0 to the head"<<endl;
     game.headPush(0);
     game.display();
     
     cout<<"Add 6 to the tail"<<endl;
     game.tailPush(6);
     game.display();
     
     cout<<"Remove head node"<<endl;
     game.headPop();
     game.display();
     
     cout<<"Remove tail node"<<endl;
     game.tailPop();
     game.display();
     
     cout<<"delete node that has number 3"<<endl;
	 game.deleteNode(3);
     game.display();
     
     cout<<"Add node number 5 to the tail"<<endl;
     game.tailPush(5);
     game.display();
     
     cout<<"delete node that has number 5"<<endl;
     game.deleteNode(5);
     game.display();
     
     cout<<"\n1 = TRUE" << endl;
     cout<<"0 = FALSE" << endl;
     cout<<"Is 5 inlist ? "<<game.isInList(5)<<endl;
     cout<<"Is 1 inlist ? "<<game.isInList(1)<<endl;
}
